# TTK 8 IoT AMS unit

In this project I would like to learn more about circuit and PCB design for a full sized embedded system.

To do this I will design and produce an IoT AMS unit that will read out an AMS meter and report the data to a
cloud solution in "real-time". This will be targeted to consumers and will be open sourced with a permissive license
so that other people can use my schematics and code to implement their own version of the unit.

In this project I will start by writing a set of design requirements that will mimic a set of "stakeholder" requirements,
then based on these requirements I will create a set of design requirements which will in turn evolve into a design specification.
This design specification is then used to create the implementation of the mentioned embedded system. The implementation will then
be tested against the design requirements which in the end will tell if the product is within acceptable margins.

I will also develop firmware for this project to make it at least have the most basic functionality described in the specification.

The product itself shall adhere to the following loose specification:

* It shall be a single unit.
* It shall communicate to a MQTT broker.
* It shall make very few assumptions about existing infrastructure.
* It shall be battery powered and rechargable.
* It shall read out power usage from the AMS meter and publish this in "real-time".
* It shall be small enough to fit inside a fuse box.
* It should be possible to record and store power failures.
* It should be possible to configure the device wihtout access to development tools(f.ex. programmers).

