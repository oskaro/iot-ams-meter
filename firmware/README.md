# IoT AMS Meter Firmware

## Project folder structure

Below is short explanation of the files in this project.

```
├── CMakeLists.txt
├── components                 Components that should be more standalone
│   ├── dlms_protocol
│   ├── ...
│   └── other_component
├── main
│   ├── CMakeLists.txt
│   └── main.c
├── Makefile                   Makefile to easily run build scripts
└── README.md                  This is the file you are currently reading
```

## Setting up build environment

Install the ESP IDF toolchain.

## How to build

```
idf.py build
```

## Flashing

```
idf.py flash -d /dev/ttyUSB<N>
```

