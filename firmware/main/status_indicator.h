#ifndef STATUS_INDICATOR_H
#define STATUS_INDICATOR_H


typedef enum {
    STATUS_INDICATOR_WIFI_LED = 0,
    STATUS_INDICATOR_AMS_LED,
    STATUS_INDICATOR_SD_LED,
    STATUS_INDICATOR_UNUSED_LED,
    STATUS_INDICATOR_SENTINEL,
} status_indicator_t;

int status_indicator_init();

int status_indicator_blink(status_indicator_t indicator);

int status_indicator_toggle(status_indicator_t indicator);

int status_indicator_set(status_indicator_t indicator, int state);


#endif //STATUS_INDICATOR_H
