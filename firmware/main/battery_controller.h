#ifndef BATTERY_CONTROLLER_H
#define BATTERY_CONTROLLER_H

#include "stdint.h"

#define V_BAT_PIN 10

int battery_controller_init();

float get_battery_level();

#endif //BATTERY_CONTROLLER_H
