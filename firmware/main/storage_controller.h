#ifndef STORAGE_CONTROLLER_H
#define STORAGE_CONTROLLER_H

#include "stdint.h"

#include "config.h"

int storage_controller_init();

int storage_controller_get_config(config_t *config);

int storage_controller_report_power_fail(uint64_t timestamp);

int storage_controller_get_firmware_image();


#endif //STORAGE_CONTROLLER_H
