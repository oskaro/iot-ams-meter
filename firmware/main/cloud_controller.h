#ifndef CLOUD_CONTROLLER_H
#define CLOUD_CONTROLLER_H

#include <stdlib.h>

typedef enum {
    CLOUD_DATA_POWER,
    CLOUD_DATA_DISCOVERY,
    CLOUD_DATA_RSSI,
    CLOUD_DATA_BATTERY,
} cloud_data_t;

typedef struct {
    cloud_data_t type;
    char *data;
    size_t len;
} cloud_packet_t;

int cloud_controller_init();

/** @brief publish data of type *type* to the cloud
 *
 */
int cloud_controller_publish(cloud_packet_t packet);


#endif // CLOUD_CONTROLLER_H
