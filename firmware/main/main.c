#include <stdio.h>
#include <string.h>

#include "sdkconfig.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_log.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_efuse.h"

#include "nvs_flash.h"


#include "battery_controller.h"
#include "ams_controller.h"
#include "wifi_controller.h"
#include "cloud_controller.h"
#include "storage_controller.h"
#include "status_indicator.h"
#include "config.h"

static char *TAG = "main";

void rssi_task(void *unused) {
    char rssi[5] = {0};
    cloud_packet_t packet = {
        .type = CLOUD_DATA_RSSI,
        .data = rssi,
        .len = sizeof(rssi),
    };
    for(;;) {
        ESP_LOGI("RSSI", "Sending RSSI");
        packet.len = sprintf(rssi, "%d", wifi_controller_get_rssi()); 
        cloud_controller_publish(packet);
        vTaskDelay(20000 / portTICK_RATE_MS);
    }
}

void battery_task(void *unused) {
    char battery_voltage[8] = {0};
    cloud_packet_t packet = {
        .type = CLOUD_DATA_BATTERY,
        .data = battery_voltage,
        .len = sizeof(battery_voltage)
    };
    
    for(;;) {
        status_indicator_blink(STATUS_INDICATOR_UNUSED_LED);
        ESP_LOGI("battery_controller", "Sending battery status");
        packet.len = sprintf(battery_voltage, "%.2f", get_battery_level());
        ESP_LOGI("battery_controller", "%s", battery_voltage);
        cloud_controller_publish(packet);
        vTaskDelay(30000 / portTICK_RATE_MS);
    }
}

static config_t config = {0};

void app_main(void)
{
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    ret = status_indicator_init();
    ESP_ERROR_CHECK(ret);

    status_indicator_blink(STATUS_INDICATOR_UNUSED_LED);

    ret = battery_controller_init();
    ESP_ERROR_CHECK(ret);

    ret = storage_controller_init();
    ESP_ERROR_CHECK(ret);

    ret = storage_controller_get_config(&config);
    ESP_ERROR_CHECK(ret);
    status_indicator_set(STATUS_INDICATOR_UNUSED_LED, true);

    ESP_LOGI(TAG, "Got SSID and password: (%s), (%s)", config.ssid, config.password);

    ret = wifi_controller_init(&config);
    ESP_ERROR_CHECK(ret);
    ESP_LOGI(TAG, "Wifi started properly");

    ret = cloud_controller_init();
    ESP_ERROR_CHECK(ret);
    ESP_LOGI(TAG, "Cloud Controller started properly");

    ret = ams_controller_init();
    ESP_ERROR_CHECK(ret);

    xTaskCreate(
        rssi_task,
        "RSSI Task",
        2048,
        NULL,
        (2 | portPRIVILEGE_BIT),
        NULL
    );

    xTaskCreate(
        battery_task,
        "Battery monitor task",
        2048,
        NULL,
        (2 | portPRIVILEGE_BIT),
        NULL
    );

}
