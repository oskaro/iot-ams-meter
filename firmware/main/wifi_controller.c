#include "wifi_controller.h"

#include <freertos/FreeRTOS.h>
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_wifi.h"
#include "esp_log.h"

#include <string.h>

#include "password.h"
#include "config.h"

static const char* TAG = "wifi_controller";

#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT BIT1
static EventGroupHandle_t s_wifi_event_group;

int wifi_controller_get_rssi() {
    wifi_ap_record_t info = {0};
    esp_wifi_sta_get_ap_info(&info);
    return info.rssi;
}

static int s_retry_num = 0;
static void wifi_event_handler(void* arg, esp_event_base_t event_base,
        int32_t event_id, void* event_data) {
    ESP_LOGI(TAG, "WIFI_EVENT base:%s, id:%d", event_base, event_id);
    if(event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if(event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        if(s_retry_num < 10) {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "Retrying to connect to AP");
        } else {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "Got IP: " IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        // TODO start a timer before reconnect!
        esp_wifi_connect();
    }
}

static int wifi_connect(const config_t *conf) {
    ESP_LOGD(TAG, "Creating event group");
    s_wifi_event_group = xEventGroupCreate();

    ESP_LOGD(TAG,"Initializing ESP net library");
    ESP_ERROR_CHECK(esp_netif_init());

    ESP_LOGD(TAG,"Create ESP event loop");
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    ESP_LOGD(TAG,"Creating Default Wifi STA");
    esp_netif_t *sta_netif = esp_netif_create_default_wifi_sta();
    assert(sta_netif);

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;

    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                    ESP_EVENT_ANY_ID,
                    &wifi_event_handler,
                    NULL,
                    &instance_any_id));

    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                    IP_EVENT_STA_GOT_IP,
                    &wifi_event_handler,
                    NULL,
                    &instance_got_ip));

    ESP_LOGD(TAG, "Creating Wi-Fi config");
    wifi_config_t wifi_config = {
        .sta = {
            .threshold.authmode = WIFI_AUTH_WPA2_PSK,
            .pmf_cfg = {
                .capable = true,
                .required = false,
            }
        }
    };

    strcpy((char*)wifi_config.sta.ssid, conf->ssid);
    strcpy((char*)wifi_config.sta.password, conf->password);

    ESP_LOGI(TAG, "Connecting to wifi using ssid: %s, password: %s", wifi_config.sta.ssid, wifi_config.sta.password);

    ESP_LOGD(TAG, "Setting Station mode and connecting");
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    if ( bits & WIFI_CONNECTED_BIT ) {
        printf("Wifi connected!\n");
        return ESP_OK;
    } else if (bits & WIFI_FAIL_BIT) {
        printf("Wifi connect failed\n");
        return -1;
    } else {
        printf("WTF?\n");
        return -2;
    }
}

int wifi_controller_init(const config_t *config) {
    return wifi_connect(config);
}

