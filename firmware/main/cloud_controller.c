#include "cloud_controller.h"

#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>

#include "mqtt_client.h"

#include "esp_log.h"

#include "status_indicator.h"

static const char* TAG = "cloud";

static esp_mqtt_client_handle_t mqtt_client;

static QueueHandle_t data_queue;

static char* topic_map[] = {
    [CLOUD_DATA_POWER] = "power/%s/usage/raw",
    [CLOUD_DATA_RSSI] = "power/%s/rssi",
    [CLOUD_DATA_BATTERY] = "power/%s/battery_voltage",
    [CLOUD_DATA_DISCOVERY] = "homeassistant/sensor/%s/config"
};

#define DISCOVERY_MESSAGE_TEMPLATE "{\"device_class\":\"%s\",\"name\":\"%s\",\"unit_of_measurement\": \"%s\",\"state_topic\": \"%s\",\"unique_id\":\"%s\",\"device\":{\"name\":\"AMS Meter\",\"identifiers\":\"ams-iot-meter\",\"model\":\"iot-ams-meter_rev1\"}}"


#define CONFIG_MQTT_TOPIC_BUF_SIZE (128)

#if CONFIG_MQTT_USE_TLS
#include "certs.h"
static char ca_cert[] = CA_CERT;
static char client_cert[] = CLIENT_CERT;
static char client_key[] = CLIENT_KEY;
#endif



static void mqtt_event_handler(void* handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    char buffer[512] = {0};
    char topic_buffer[128] = {0};
    size_t data_len = 0;
    switch((esp_mqtt_event_id_t)event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGD(TAG, "MQTT_EVENT_CONNECTED");


            sprintf(topic_buffer, topic_map[CLOUD_DATA_POWER], CONFIG_MQTT_CLIENT_ID);
            ESP_LOGD(TAG, "Topic: %s", topic_buffer);
            data_len = sprintf(buffer, DISCOVERY_MESSAGE_TEMPLATE, "power", "Power usage", "W", topic_buffer, CONFIG_MQTT_CLIENT_ID"-power"); 
            ESP_LOGE(TAG, "DATA SENT: %s", buffer);
             sprintf(topic_buffer, topic_map[CLOUD_DATA_DISCOVERY], CONFIG_MQTT_CLIENT_ID "-power");
            msg_id = esp_mqtt_client_publish(client, topic_buffer, buffer, data_len, 2, 0);
            ESP_LOGI(TAG, "Sent discovery message successfully, topic='%s' msg_id=%d", topic_buffer, msg_id);

            sprintf(topic_buffer, topic_map[CLOUD_DATA_RSSI], CONFIG_MQTT_CLIENT_ID);
            ESP_LOGD(TAG, "Topic: %s", topic_buffer);
            data_len = sprintf(buffer, DISCOVERY_MESSAGE_TEMPLATE, "signal_strength", "RSSI", "dB", topic_buffer, CONFIG_MQTT_CLIENT_ID "-rssi"); 
            ESP_LOGE(TAG, "DATA SENT: %s", buffer);
            sprintf(topic_buffer, topic_map[CLOUD_DATA_DISCOVERY], CONFIG_MQTT_CLIENT_ID "-rssi");
            msg_id = esp_mqtt_client_publish(client, topic_buffer, buffer, data_len, 2, 0);
            ESP_LOGI(TAG, "Sent discovery message successfully, topic='%s' msg_id=%d", topic_buffer, msg_id);

            sprintf(topic_buffer, topic_map[CLOUD_DATA_BATTERY], CONFIG_MQTT_CLIENT_ID);
            ESP_LOGD(TAG, "Topic: %s", topic_buffer);
            data_len = sprintf(buffer, DISCOVERY_MESSAGE_TEMPLATE, "battery", "Battery voltage", "V", topic_buffer, CONFIG_MQTT_CLIENT_ID "-battery"); 
            ESP_LOGE(TAG, "DATA SENT: %s", buffer);
            sprintf(topic_buffer, topic_map[CLOUD_DATA_DISCOVERY], CONFIG_MQTT_CLIENT_ID "-battery");
            msg_id = esp_mqtt_client_publish(client, topic_buffer, buffer, data_len, 2, 0);
            ESP_LOGI(TAG, "Sent discovery message successfully, topic='%s' msg_id=%d", topic_buffer, msg_id);
            
            break;

        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGD(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;

        case MQTT_EVENT_PUBLISHED:
            status_indicator_blink(STATUS_INDICATOR_WIFI_LED);
            ESP_LOGD(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;

        case MQTT_EVENT_DATA:
            status_indicator_blink(STATUS_INDICATOR_WIFI_LED);
            ESP_LOGD(TAG, "MQTT_EVENT_DATA");
            ESP_LOGD(TAG,"TOPIC=%.*s\r\n", event->topic_len, event->topic);
            ESP_LOGD(TAG,"DATA=%.*s\r\n", event->data_len, event->data);
            break;

        case MQTT_EVENT_ERROR:
            ESP_LOGE(TAG, "MQTT_EVENT_ERROR");
            break;

        case MQTT_EVENT_BEFORE_CONNECT:
            ESP_LOGD(TAG, "MQTT_EVENT_BEFORE_CONNECT");
            break;

        default:
            ESP_LOGW(TAG, "other event id: %d", event->event_id);
            break;
    }
}

static void task_runner() {
    cloud_packet_t data = {0};
    char topic_buffer[CONFIG_MQTT_TOPIC_BUF_SIZE] = {0};

    for(;;) {
        if(xQueueReceive(data_queue, &data, portMAX_DELAY) == pdPASS) {
            // TODO maybe parse the data and make strings of them?
            ESP_LOGD(TAG, "Got data");

            sprintf(topic_buffer, topic_map[data.type], CONFIG_MQTT_CLIENT_ID);
            ESP_LOGI(TAG, "Publishing data to %s", topic_buffer);

            esp_mqtt_client_publish(
                    mqtt_client, 
                    topic_buffer, 
                    data.data, data.len,
                    0, 0);
        } else {
            ESP_LOGW(TAG, "Timed out waiting for cloud data");
        }
    } 
}

static void mqtt_connect() {
    esp_mqtt_client_config_t mqtt_config = {
        // TODO this goes into some config some place
        // and should be able to be extracted from SD card.
        .uri = CONFIG_MQTT_URI,
        .client_id = CONFIG_MQTT_CLIENT_ID,
        .port = CONFIG_MQTT_PORT,

#if CONFIG_MQTT_USE_TLS
        .client_cert_pem = client_cert,
        .client_cert_len = sizeof(client_cert),
        .cert_pem = ca_cert,
        .cert_len = sizeof(ca_cert),
        .client_key_pem = client_key,
        .client_key_len = sizeof(client_key),
#else
        .username = CONFIG_MQTT_USERNAME,
        .password = CONFIG_MQTT_PASSWORD,
#endif
    };

    ESP_LOGD(TAG, "Initializing MQTT client");
    mqtt_client = esp_mqtt_client_init(&mqtt_config);
    esp_mqtt_client_register_event(mqtt_client, ESP_EVENT_ANY_ID, mqtt_event_handler, NULL);

    ESP_LOGD(TAG, "Starting MQTT client");
    esp_mqtt_client_start(mqtt_client);
}

int cloud_controller_publish(cloud_packet_t data) {
    return xQueueSend(data_queue, &data, 0);
}

int cloud_controller_init() {
    data_queue = xQueueCreate(8, sizeof(cloud_packet_t));

    ESP_LOGI(TAG, "Connecting to MQTT");
    mqtt_connect();

    xTaskCreate(
        task_runner,
        "Cloud Controller",
        2048,
        NULL,
        (2 | portPRIVILEGE_BIT),
        NULL
    );
    return ESP_OK;
}

