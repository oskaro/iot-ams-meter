#include "storage_controller.h"
#include "status_indicator.h"
#include "config.h"

#include <esp_vfs_fat.h>
#include "sdmmc_cmd.h"

static const char *TAG = "storage";

#define MOUNT_POINT "/sdcard"

// IO16
#define PIN_MISO 16
// IO18
#define PIN_MOSI 18
// IO17
#define PIN_CLK 17
// IO19
#define PIN_CS 19
 
#define SPI_DMA_CHAN host.slot

static esp_vfs_fat_mount_config_t mount_config = {
    .format_if_mount_failed = true,
    .max_files = 5,
    .allocation_unit_size = 16*1024
};
static sdmmc_card_t *card;
static const char mount_point[] = MOUNT_POINT;
static sdmmc_host_t host = SDSPI_HOST_DEFAULT();
static spi_bus_config_t bus_cfg = {
    .mosi_io_num = PIN_MOSI,
    .miso_io_num = PIN_MISO,
    .sclk_io_num = PIN_CLK,
    .quadwp_io_num = -1,
    .quadhd_io_num = -1,
    .max_transfer_sz = 4000,
};

static sdspi_device_config_t slot_config = SDSPI_DEVICE_CONFIG_DEFAULT();

int storage_controller_get_firmware_image() {
    return -1;
}

int storage_controller_report_power_fail(uint64_t timestamp) {
    const char *power_fail_file = MOUNT_POINT"/pf.bin";
    FILE* fh = fopen(power_fail_file, "a+");
    if (fh == NULL) {
        ESP_LOGE(TAG, "could not open power fail file");
        return -1;
    }
    
    fwrite(&timestamp, sizeof(timestamp), 1, fh);
    fclose(fh);
    return ESP_OK;
}

int storage_controller_get_config(config_t *config) {
    int ret;
    const char *config_file = MOUNT_POINT"/config.bin";
    FILE *fh = fopen(config_file, "rb");

    if (fh == NULL) {
        ESP_LOGE(TAG, "Could not open config file!");
        return -1;
    }

    uint8_t ssid_len = 0;
    ret = fread(&ssid_len, sizeof(ssid_len), 1, fh);
    if (ret != 1) {
        ESP_LOGE(TAG, "Could not read SSID length from config file");
        return -1;
    }

    fgets(config->ssid, ssid_len + 1, fh);

    uint8_t passwd_len = 0;
    ret = fread(&passwd_len, sizeof(passwd_len), 1, fh);
    if (ret != 1) {
        ESP_LOGE(TAG, "Could not read password length from config file");
        return -1;
    }

    fgets(config->password, passwd_len + 1, fh);

    fclose(fh);
    return ESP_OK;
}

int storage_controller_init() {
    status_indicator_blink(STATUS_INDICATOR_SD_LED);
    esp_err_t ret;
    ESP_LOGI(TAG, "Initializing SD card");
    ret = spi_bus_initialize(host.slot, &bus_cfg, SPI_DMA_CHAN);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed to initialize SPI bus");
        return ret;
    }

    slot_config.gpio_cs = PIN_CS;
    slot_config.host_id = host.slot;

    ESP_LOGI(TAG, "Mounting filesystem");

    ret = esp_vfs_fat_sdspi_mount(mount_point, &host, &slot_config, &mount_config, &card);

    if ( ret != ESP_OK ) {
        if (ret == ESP_FAIL) {
            ESP_LOGE(TAG, "Failed to mount file system");
        } else {
            ESP_LOGE(TAG, "Failed to initialize the card (%s)", esp_err_to_name(ret));
        }
        return ret;
    }

    ESP_LOGI(TAG, "Filesystem mounted");

    return ESP_OK;

}
