#include "battery_controller.h"

#include "driver/adc_common.h"
#include "esp_log.h"
#include "hal/adc_types.h"
#include "esp_adc_cal.h"
#include "esp_efuse.h"

static const char *TAG = "battery_controller";

static esp_adc_cal_characteristics_t chars = {0};

#define BATTERY_ADC_CHANNEL ADC1_CHANNEL_9
#define BATTERY_ADC_ATTEN ADC_ATTEN_DB_11
#define N_SAMPLES 128

#define R1 (7.8)
#define R2 (10.0)
// V_o = V_i * R2/(R1 + R2)
// V_i = V_o * (R1 + R2)/R2
#define MEAS_TO_BATTERY_VOLTAGE ((R1 + R2)/R2)

int battery_controller_init() {
    if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP) == ESP_OK) {
        ESP_LOGI(TAG, "eFuse Two Point: Supported");
    } else {
        ESP_LOGW(TAG, "Cannot retrieve eFuse Two Point calibration values. Default calibration values will be used.");
    }

    adc1_config_width(ADC_WIDTH_BIT_13);
    adc1_config_channel_atten(BATTERY_ADC_CHANNEL, BATTERY_ADC_ATTEN);

    int calibration_value = esp_adc_cal_characterize(ADC_UNIT_1, BATTERY_ADC_ATTEN, ADC_WIDTH_BIT_13, 0, &chars);

    switch(calibration_value) {
        case ESP_ADC_CAL_VAL_DEFAULT_VREF:
            break;
        case ESP_ADC_CAL_VAL_EFUSE_TP:
            break;
        case ESP_ADC_CAL_VAL_EFUSE_VREF:
            break;
        default:
            ESP_LOGW(TAG,"Unknown calibration value: %d", calibration_value);
    }

    return ESP_OK;
}

float get_battery_level() {
    uint32_t raw = 0;
    uint32_t bat_mv = 0;

    for(int i = 0; i < N_SAMPLES; i++) {
        raw += adc1_get_raw(BATTERY_ADC_CHANNEL);
    }
    raw /= N_SAMPLES;
    bat_mv = esp_adc_cal_raw_to_voltage(raw, &chars);
    ESP_LOGD(TAG, "'Raw: %d, Battery measurement: %d",raw, bat_mv);

    float battery_v = (bat_mv * MEAS_TO_BATTERY_VOLTAGE) / 1000.0;

    return battery_v;
}
