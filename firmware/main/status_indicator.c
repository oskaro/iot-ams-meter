#include "status_indicator.h"

#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"

#include "driver/gpio.h"

#include"esp_log.h"

#include "stdint.h"

const static char TAG[] = "status_indicator";

#define BLINK_TICKS (300 / portTICK_RATE_MS)

#define WIFI_LED        42
#define AMS_LED         39
#define SD_LED          40
#define UNUSED_LED      41

#define ALL_LED_BITMASK ((1ULL << WIFI_LED) | (1ULL << AMS_LED) | (1ULL << SD_LED) | (1ULL << UNUSED_LED))
 
const static uint64_t GPIO_MAP[] = {
    [STATUS_INDICATOR_WIFI_LED] = WIFI_LED,
    [STATUS_INDICATOR_AMS_LED] = AMS_LED,
    [STATUS_INDICATOR_SD_LED] = SD_LED,
    [STATUS_INDICATOR_UNUSED_LED] = UNUSED_LED,
};

static int led_state[STATUS_INDICATOR_SENTINEL] = {0};

static TimerHandle_t timer_array[STATUS_INDICATOR_SENTINEL];

int status_indicator_blink(status_indicator_t indicator) {
    int ret = 0;
    status_indicator_set(indicator, true);

    ret = xTimerStart(timer_array[indicator], 0);
    if (ret != pdPASS) {
        ESP_LOGE(TAG, "Could not start timer for status indicator");
        return -1;
    }
    return ESP_OK;
}
 
static int status_indicator_get(status_indicator_t indicator) {
    return led_state[indicator];
}

int status_indicator_toggle(status_indicator_t indicator) {
    int state = status_indicator_get(indicator);
    if (state == 0) {
        return status_indicator_set(indicator, 1);
    } else {
        return status_indicator_set(indicator, 0);
    }
}


int status_indicator_set(status_indicator_t indicator, int state) {
    led_state[indicator] = state;
    return gpio_set_level(GPIO_MAP[indicator], state );
}


static void prvTimerCallback( TimerHandle_t indicator) {
    status_indicator_t id = (status_indicator_t)pvTimerGetTimerID(indicator);
    if (id < 0 || id >= STATUS_INDICATOR_SENTINEL) {
        ESP_LOGE(TAG, "Got invalid ID, got %d", id);
        return;
    }

    status_indicator_set(id, false);
}

int status_indicator_init() {
    gpio_config_t io_conf = {
        .intr_type = GPIO_INTR_DISABLE,
        .mode = GPIO_MODE_OUTPUT,
        .pin_bit_mask = ALL_LED_BITMASK,
        .pull_down_en = 0,
        .pull_up_en = 0,
    };

    int ret = gpio_config(&io_conf);
    if( ret != ESP_OK ) {
        ESP_LOGE(TAG, "Could not configure GPIO");
        return ret;
    }

    for(int indicator = 0; indicator < STATUS_INDICATOR_SENTINEL; ++indicator) {
        ESP_LOGI(TAG, "Creating timer for indicator: %d", indicator);
        timer_array[indicator] = xTimerCreate(
                "Timer",
                BLINK_TICKS,
                pdFALSE,
                (void*) indicator,
                prvTimerCallback
            );
        if (timer_array[indicator] == NULL) {
            ESP_LOGE(TAG, "Could not create timer %d", indicator);
        }

    }

    return ESP_OK;
}
