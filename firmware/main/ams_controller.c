#include "ams_controller.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include <time.h>

#include "esp_log.h"
#include "esp_sntp.h"

#include "driver/gpio.h"
#include "driver/uart.h"

#include <dlms_protocol.h>
#include "cloud_controller.h"
#include "storage_controller.h"

static const char* TAG = "AMS";

#define AMS_RX_PIN 15
#define AMS_PF_PIN 14

#define BUF_SIZE (512)
#define RD_BUF_SIZE (BUF_SIZE)
static QueueHandle_t modbus_queue;

static uint8_t read_buffer[RD_BUF_SIZE] = {0};

static dlms_frame_t latest_frame = {0};

static gpio_config_t power_failure_gpio_config = {
    .intr_type = GPIO_INTR_NEGEDGE,
    .mode = GPIO_MODE_INPUT,
    .pin_bit_mask = (1UL << AMS_PF_PIN),
    .pull_down_en = 0,
    .pull_up_en = 0,
};


static  uart_config_t modbus_uart_config = {
    .baud_rate = 2400,
    .data_bits = UART_DATA_8_BITS,
    .parity = UART_PARITY_DISABLE,
    .stop_bits = UART_STOP_BITS_1,
    .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
    .source_clk = UART_SCLK_APB,
};

static void power_failure_handler(void* args) {
    struct timeval tv_now;

    gettimeofday(&tv_now, NULL);

    int64_t time_us = (int64_t)tv_now.tv_sec * 1000000L + (int64_t)tv_now.tv_usec;

    storage_controller_report_power_fail(time_us);
}

static void modbus_task( void * pvParameters ) {
    int ret = 0;
    uart_event_t event = {0};
    char power_usage[10] = {0};

    cloud_packet_t packet = {
        .type = CLOUD_DATA_POWER,
        .data = power_usage,
        .len = sizeof(power_usage)
    };

    uint8_t *frame_buffer = NULL;
    size_t read_index = 0;

    for (;;) {
        ESP_LOGD(TAG, "Waiting for modbus message");

        if(xQueueReceive(modbus_queue, (void *) &event, (portTickType)portMAX_DELAY)) {
            ESP_LOGD(TAG, "Got event type: %d, size: %zd", event.type, event.size);

            switch(event.type) {
                case UART_DATA:
                    ESP_LOGD(TAG, "read_index = %zd, event.size = %zd, sizeof(read_buffer) = %i", read_index, event.size, sizeof(read_buffer));
                    if (read_index + event.size > sizeof(read_buffer)) {
                        ESP_LOGE(TAG, "Read will write out of bounds, resetting buffer");
                        read_index = 0;
                    }

                    // Append the new bytes to the read buffer
                    size_t _data_read = uart_read_bytes(UART_NUM_1, &read_buffer[read_index], event.size, portMAX_DELAY);
                    ESP_LOGD(TAG, "Read %zd bytes from UART buffer", _data_read);
                    

                    for(int i = 0; i < sizeof(read_buffer) - 1; ++i) {
                        // This will very likely be in the very beginning of the buffer.
                        if(read_buffer[i] == DLMS_FRAME_START_BYTE
                                && read_buffer[i + 1] != DLMS_FRAME_START_BYTE) {

                            frame_buffer = &read_buffer[i + 1];
                            break;
                        }
                    }
                    if (frame_buffer == NULL) {
                        ESP_LOGE(TAG, "No start byte found!");
                        continue;
                    }

                    ESP_LOGD(TAG, "read_buffer = %p, frame_buffer = %p, frame offset = %d",read_buffer, frame_buffer, frame_buffer - read_buffer);

                    int header_size = hdlc_parse_header(&latest_frame.header, frame_buffer, read_index + event.size);
                    if (header_size < 0) {
                        ESP_LOGE(TAG, "Could not parse AMS header, error code: %d", header_size);
                        break;
                    }
                    ESP_LOGD(TAG, "Parsed HDLC header, with size: %zd", header_size);

                    // This includes the start and stop bytes, header, DLMS frame and the footer.
                    size_t frame_len = hdlc_get_frame_length(&latest_frame.header);
                    if(read_index + event.size < frame_len + 2) {
                        ESP_LOGW(TAG, "Entire frame is not read.");
                        // TODO check that the buffer is large enough
                        read_index += event.size;
                        break;
                    }
                    if (frame_buffer[frame_len] != DLMS_FRAME_START_BYTE) {
                        ESP_LOGE(TAG, "Expected end of frame not found, found: %d, frame_len = %zd", frame_buffer[frame_len], frame_len);
                    }
                    // We have read an entire frame
                    // TODO check if we can reset the entire buffer or there is another
                    // frame in the buffer.
                    read_index = 0;

                    // We free the frame before we reuse the struct to not leave dangling pointers.
                    dlms_free_frame(&latest_frame);
                    ret = dlms_parse_body(&latest_frame, &frame_buffer[header_size], frame_len - header_size);
                    if (ret < 0) {
                        ESP_LOGE(TAG, "could not parse DLMS body");
                        read_index = 0;
                        break;
                    }

                    ret = hdlc_parse_footer(&latest_frame.footer, frame_buffer, &frame_buffer[header_size + ret], frame_len);
                    if (ret < 0) {
                        ESP_LOGE(TAG, "could not parse footer corretly: %d", ret);
                        break;
                    }

                    if (latest_frame.objects.data.size == 1) {
                        packet.len = sprintf(power_usage, "%d", latest_frame.objects.data.elements[0].bytes);
                        cloud_controller_publish(packet);
                    } 

                    read_index = 0;
                    frame_buffer = NULL;
                    break;
                default:
                    ESP_LOGD(TAG,"[DONT CARE]\n");
                    
            }
        }
    }
    vTaskDelete(NULL);
}

int ams_controller_init() {
    ESP_LOGI(TAG, "Configuring UART driver");
    esp_err_t ret;
    ret = uart_driver_install(UART_NUM_1,  BUF_SIZE * 2,0 , 20, &modbus_queue, 0);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Could not install UART driver");
        return ret;
    }

    ret = uart_param_config(UART_NUM_1, &modbus_uart_config);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Could not configure UART");
        return ret;
    }

    ret = uart_set_pin(UART_NUM_1, -1, AMS_RX_PIN, -1, -1);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Could not set UART pins");
        return ret;
    }

    ret = gpio_config(&power_failure_gpio_config);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Could not set up power failure GPIO pin!");
        return ret;
    }

    ret = gpio_install_isr_service(0);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Could not enable GPIO ISR service");
        if (ret == ESP_ERR_INVALID_STATE) {
            ESP_LOGE(TAG, "GPIO ISR service already initialized!");
            // TODO is this a problem?
        }
        return ret;
    }

    ret  = gpio_isr_handler_add(AMS_PF_PIN, power_failure_handler, (void *) AMS_PF_PIN);

    xTaskCreate(
        modbus_task,
        "AMS Task",
        4096,
        NULL,
        (2 | portPRIVILEGE_BIT),
        NULL
    );

    return ESP_OK;
}

