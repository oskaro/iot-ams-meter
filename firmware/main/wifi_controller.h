#ifndef WIFI_CONTROLLER_H
#define WIFI_CONTROLLER_H

#include "config.h"

int wifi_controller_init(const config_t *config);

int wifi_controller_get_rssi(void);


#endif //WIFI_CONTROLLER_H
