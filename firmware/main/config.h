#ifndef CONFIG_H
#define CONFIG_H

#include <stdlib.h>

typedef enum {
    CLIENT_CERTIFICATE,
    CLIENT_KEY,
    ROOT_CA,
} config_entry_t;

typedef struct {
    char ssid[32];
    char password[64];
} config_t;

#endif //CONFIG_H
