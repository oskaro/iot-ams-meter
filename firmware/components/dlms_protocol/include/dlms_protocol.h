#ifndef AMS_PROTOCOL_H
#define AMS_PROTOCOL_H

#include <stdint.h>
#include <stdlib.h>

#define DLMS_FRAME_START_BYTE 0x7E


typedef enum {
    DLMS_OK = 0,
    //< Start off error messages
    DLMS_ERR = 128,
    //< No start of frame is found
    DLMS_ERR_NO_FRAME_START,
    //< No end of frame is found
    DLMS_ERR_NO_FRAME_END,
    //< A data field is invalid.
    DLMS_ERR_INVALID_DATA,
    //< The header data is corrupt
    DLMS_ERR_CORRUPT_HEADER,
    //< The body data is corrupt
    DLMS_ERR_CORRUPT_BODY,
    //< We run out of memory when trying to parse frame.
    DLMS_ERR_OOM,
} dlms_error_t;

#define HDLC_MAX_SERVER_ADDRESS_LENGTH (4)
#define HDLC_MIN_HEADER_LENGTH (7)

typedef struct {
    uint16_t frame_format;

    uint8_t dest_address;

    // If the last bit in an address field
    // is 0 it means that the next byte
    // extends the address.
    uint8_t src_address[HDLC_MAX_SERVER_ADDRESS_LENGTH];
    size_t src_address_len;

    uint8_t control_field;
    uint16_t header_check_sequence;

} hdlc_header_t;

#define HDLC_FOOTER_SIZE (2)

typedef struct {
    uint16_t frame_check_sequence;
} hdlc_footer_t;

typedef struct {
    uint8_t dest_lsap;
    uint8_t source_lsap;
    uint8_t llc_quality;
    uint8_t unparsed_data_1[7];
    uint16_t year;
    uint8_t month;
    uint8_t day_of_month;
    uint8_t weekday;
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
    uint8_t hundreth_of_a_second;
    uint16_t utc_offset;
    uint8_t clock_status;
} dlms_information_t;

typedef enum {
    OBIS_TYPE_STRUCT = 0x02,
    OBIS_TYPE_VISIBLE_STRING = 0x4,
    OBIS_TYPE_UNSIGNED_4_BYTES = 0x06,
    OBIS_TYPE_OCTET_STRING = 0x09,
    OBIS_TYPE_UNSIGNED_2_BYTES = 0x12,
} obis_object_type_t;

typedef struct obis_object obis_object_t;

typedef struct {
    uint8_t size;
    obis_object_t *elements;
} obis_struct_t;

typedef struct {
    uint8_t len;
    char *string;
} obis_visible_string_t;

typedef struct {
    uint8_t len;
    uint8_t *string;
} obis_octet_string_t;

struct obis_object {
    obis_object_type_t type;
    union {
        obis_struct_t data;
        obis_visible_string_t string;
        obis_octet_string_t octet_string;
        /// If the type is bytes this must be cast to the correct
        /// integer type
        uint32_t bytes;
    };
};

typedef struct {
    hdlc_header_t header;
    dlms_information_t body;
    obis_object_t objects;
    hdlc_footer_t footer;
} dlms_frame_t;

/***
 * @brief Take a byte array and decode the DLMS message from it.
 *
 * If two frames are loaded into the same buffer this function will just decode
 * the first frame and then return the amount of bytes that it has "consumed"
 * The user is responsible for freeing the frame after it is created.
 * @returns 0 if decoding succeeded, negative error if it failed.
 */
int dlms_parse_body(dlms_frame_t *out, uint8_t *bytes, size_t len);

/***
 * @brief Free a DLMS frame
 */
int dlms_free_frame(dlms_frame_t *frame);

int hdlc_parse_header(hdlc_header_t *output, uint8_t *frame, size_t len);

int hdlc_get_frame_length(hdlc_header_t *header);

int hdlc_parse_footer(hdlc_footer_t *footer, uint8_t *frame_head, uint8_t *footer_head, size_t frame_len);

#endif
