#include "dlms_protocol.h"

#include <esp_log.h>

#include <stdint.h>
#include <string.h>

static char TAG[] = "DLMS_PARSER";

#define HDLC_FRAME_TYPE_BIT_MASK   (0xf000)
#define HDLC_SEQUENCE_BIT_MASK     (0x0800)
#define HDLC_FRAME_LENGTH_BIT_MASK (0x07ff)

#define HDLC_FRAME_TYPE_SHIFT   (12)
#define HDLC_SEQUENCE_BIT_SHIFT (11)
#define HDLC_FRAME_LENGTH_SHIFT (0)

#define HDLC_FRAME_LENGTH(frame_format) \
    ((frame_format & HDLC_FRAME_LENGTH_BIT_MASK) >> (HDLC_FRAME_LENGTH_SHIFT))
#define HDLC_FRAME_TYPE(frame_format) \
    ((frame_format & HDLC_FRAME_TYPE_BIT_MASK) >> (HDLC_FRAME_TYPE_SHIFT))
#define HDLC_SEQUENCE_BIT(frame_format) \
    ((frame_format & HDLC_SEQUENCE_BIT_MASK) >> (HDLC_SEQUENCE_BIT_SHIFT))


static uint8_t pop_buffer_head(uint8_t **buffer) {
    uint8_t data = (*buffer)[0];
    (*buffer)++;
    return data;
}

static uint16_t pop_buffer_head_uint16(uint8_t **buffer) {
    uint8_t *buf = *buffer;
    uint16_t data = buf[0] << 8 | buf[1];
    (*buffer) += 2;
    return data;
}

static uint32_t pop_buffer_head_uint32(uint8_t **buffer) {
    uint8_t *buf = *buffer;
    uint32_t data = buf[0] << 24 | buf[1] << 16 | buf[2] << 8 | buf[3];
    (*buffer) += 4;
    return data;
}

static uint16_t fcstab[256] = {
    0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf, 0x8c48,
    0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7, 0x1081, 0x0108,
    0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e, 0x9cc9, 0x8d40, 0xbfdb,
    0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876, 0x2102, 0x308b, 0x0210, 0x1399,
    0x6726, 0x76af, 0x4434, 0x55bd, 0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e,
    0xfae7, 0xc87c, 0xd9f5, 0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e,
    0x54b5, 0x453c, 0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd,
    0xc974, 0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
    0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3, 0x5285,
    0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a, 0xdecd, 0xcf44,
    0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72, 0x6306, 0x728f, 0x4014,
    0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9, 0xef4e, 0xfec7, 0xcc5c, 0xddd5,
    0xa96a, 0xb8e3, 0x8a78, 0x9bf1, 0x7387, 0x620e, 0x5095, 0x411c, 0x35a3,
    0x242a, 0x16b1, 0x0738, 0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862,
    0x9af9, 0x8b70, 0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e,
    0xf0b7, 0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
    0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036, 0x18c1,
    0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e, 0xa50a, 0xb483,
    0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5, 0x2942, 0x38cb, 0x0a50,
    0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd, 0xb58b, 0xa402, 0x9699, 0x8710,
    0xf3af, 0xe226, 0xd0bd, 0xc134, 0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7,
    0x6e6e, 0x5cf5, 0x4d7c, 0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1,
    0xa33a, 0xb2b3, 0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72,
    0x3efb, 0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
    0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a, 0xe70e,
    0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1, 0x6b46, 0x7acf,
    0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9, 0xf78f, 0xe606, 0xd49d,
    0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330, 0x7bc7, 0x6a4e, 0x58d5, 0x495c,
    0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};

#define PPPINITFCS16 0xffff /* Initial FCS value */
#define PPPGOODFCS16 0xf0b8 /* Good final FCS value */

static uint16_t hdlc_check_crc(const uint8_t *data, int len) {
    assert(sizeof(uint16_t) == 2);
    assert(((uint16_t)-1) > 0);

    uint16_t fcs = 0xffff;
    for(int i = 0; i < len; ++i) {
        uint8_t index = (fcs ^ data[i]) & 0xff;
        fcs = (uint16_t)((fcs >> 8) ^ fcstab[index]);
    }
    fcs ^= 0xffff;
    return (fcs);
}

/** @brief Parse an OBIS object, if it is a struct it will parse it recursively.
 *
 */
static int dlms_parse_obis_objects(obis_object_t *object, uint8_t *frame) {
    int parsed_data = 0;
    object->type = pop_buffer_head(&frame);
    ESP_LOGD(TAG, "Found type: %x", object->type);
    switch (object->type) {
        case OBIS_TYPE_STRUCT:
            object->data.size = pop_buffer_head(&frame);
            object->data.elements = malloc(object->data.size * sizeof(object->data.elements[0]));
            if (object->data.elements == NULL) {
                return -DLMS_ERR_OOM;
            }
            // Parse all the objects in the OBIS struct
            for (int i = 0; i < object->data.size; ++i) {
                parsed_data += dlms_parse_obis_objects(
                    &object->data.elements[i], frame + parsed_data);
            }

            parsed_data += 2;
            break;

        case OBIS_TYPE_OCTET_STRING:
            object->octet_string.len = pop_buffer_head(&frame);
            object->octet_string.string = malloc(object->octet_string.len * sizeof(object->octet_string.string[0]));
            parsed_data += 2;
            if (object->octet_string.string == NULL) {
                return -DLMS_ERR_OOM;
            }
            for (int i = 0; i < object->octet_string.len; ++i) {
                object->octet_string.string[i] = frame[i];
                parsed_data += 1;
            }
            break;

        case OBIS_TYPE_VISIBLE_STRING:
            object->string.len = pop_buffer_head(&frame);
            parsed_data += 2;
            object->string.string = malloc(object->string.len * sizeof(object->string.string[0]));
            if (object->string.string == NULL) {
                return -DLMS_ERR_OOM;
            }
            for (int i = 0; i < object->string.len; ++i) {
                object->string.string[i] = frame[i];
                parsed_data += 1;
            }
            break;

        case OBIS_TYPE_UNSIGNED_2_BYTES:
            object->bytes = pop_buffer_head_uint16(&frame);
            parsed_data += 3;
            break;

        case OBIS_TYPE_UNSIGNED_4_BYTES:
            object->bytes = pop_buffer_head_uint32(&frame);
            parsed_data += 5;
            break;

        default:
            ESP_LOGE(
                TAG,
                "Error when parsing OBIS object, got unrecognized value: %i",
                object->type);
            break;
    }

    return parsed_data;
}

/** @brief Parse the hdlc header of a frame and check the CRC
 *
 * @return the size of the header on success, negative error code on error.
 */
int hdlc_parse_header(hdlc_header_t *header, uint8_t *frame, size_t len) {
    uint8_t const *head = frame;

    if (len < HDLC_MIN_HEADER_LENGTH) {
        return -DLMS_ERR_INVALID_DATA;
    }

    ESP_LOGD(TAG, "poping frame_format: %p", frame);
    header->frame_format = pop_buffer_head_uint16(&frame);

    ESP_LOGD(TAG, "poping first dest_address: %p", frame);
    // Now we have validated that we have the correct amount of data and we can
    // start parsing the protocol.
    header->dest_address = pop_buffer_head(&frame);

    for (int i = 0; i < HDLC_MAX_SERVER_ADDRESS_LENGTH; ++i) {
        ESP_LOGD(TAG, "poping %d src_address: %p", i, frame);
        header->src_address[i] = pop_buffer_head(&frame);

        if ((header->src_address[i] & 0x0001)) {
            header->src_address_len = i + 1;
            break;
        }
    }

    ESP_LOGD(TAG, "poping control field: %p", frame);
    header->control_field = pop_buffer_head(&frame);

    ESP_LOGD(TAG, "poping crc field: %p", frame);
    // FCS and HCS are LE
    header->header_check_sequence = frame[0] | frame[1] << 8;
    frame += 2;

    size_t header_size = frame - head;
    ESP_LOGD(TAG, "header_size = %zd, head = %p, frame = %p", header_size, head, frame);
    uint16_t crc = hdlc_check_crc(head, header_size - 2);
    ESP_LOGD(TAG, "header_check_sequence = %x, calculated crc = %x", header->header_check_sequence, crc);
    if (crc != header->header_check_sequence) {
        return -DLMS_ERR_CORRUPT_HEADER;
    }

ESP_LOGD(TAG, "Header: frame_format= %x, dest_address = %x, src_address = %x, control_field = %x, header_check_sequence = %x", header->frame_format, header->dest_address, header->src_address[0], header->control_field, header->header_check_sequence);

    // we return the size of the header.
    return header_size;
}

int hdlc_get_frame_length(hdlc_header_t *header) {
    return HDLC_FRAME_LENGTH(header->frame_format);
}

int hdlc_parse_footer(hdlc_footer_t *footer, uint8_t *frame_head, uint8_t *footer_head, size_t frame_len) {
    // FCS and HCS are little endian
    footer->frame_check_sequence = footer_head[0] | footer_head[1] << 8;
    footer_head += 2;

    uint16_t crc = hdlc_check_crc(frame_head, frame_len - 2);
    ESP_LOGD(TAG, "FRAME HEAD = %x, last_element = %zd", frame_head[0], footer_head- frame_head);
    ESP_LOGD(TAG, "received CRC = %x, calculated CRC = %x", footer->frame_check_sequence, crc);
    if (crc != footer->frame_check_sequence) {
        return -DLMS_ERR_CORRUPT_BODY;
    }

    if (footer_head[0] != DLMS_FRAME_START_BYTE) {
        ESP_LOGE(
            TAG,
            "Start frame not found after parsing body, something went wrong");
        return -DLMS_ERR_CORRUPT_BODY;
    }

    return DLMS_OK;
}

int dlms_parse_body(dlms_frame_t *output, uint8_t *frame, size_t len) {
    uint8_t const*head = frame;
    dlms_information_t *body = &output->body;

    output->body.dest_lsap = pop_buffer_head(&frame);
    ESP_LOGD(TAG, "dest_lsap = %x", output->body.dest_lsap);
    output->body.source_lsap = pop_buffer_head(&frame);
    ESP_LOGD(TAG, "src_lsap = %x", output->body.source_lsap);
    output->body.llc_quality = pop_buffer_head(&frame);
    ESP_LOGD(TAG, "llc_quality = %x", output->body.llc_quality);

    for (int i = 0; i < sizeof(output->body.unparsed_data_1); ++i) {
        body->unparsed_data_1[i] = pop_buffer_head(&frame);
        ESP_LOGD(TAG, "unparsed_data[%i] = %x",i, output->body.unparsed_data_1[i]);
    }

    output->body.year = pop_buffer_head_uint16(&frame);
    ESP_LOGD(TAG, "year = %d", output->body.year);

    body->month = pop_buffer_head(&frame);
    ESP_LOGD(TAG, "month = %d", output->body.month);
    body->day_of_month = pop_buffer_head(&frame);
    ESP_LOGD(TAG, "day = %d", output->body.day_of_month);
    body->weekday = pop_buffer_head(&frame);
    ESP_LOGD(TAG, "weekday = %d", output->body.weekday);
    body->hour = pop_buffer_head(&frame);
    ESP_LOGD(TAG, "hour = %d", output->body.hour);
    body->minute = pop_buffer_head(&frame);
    ESP_LOGD(TAG, "minute = %d", output->body.minute);
    body->second = pop_buffer_head(&frame);
    ESP_LOGD(TAG, "sec = %d", output->body.second);
    body->hundreth_of_a_second = pop_buffer_head(&frame);
    ESP_LOGD(TAG, "hundreth_of_a_second = %x", output->body.hundreth_of_a_second);

    body->utc_offset = pop_buffer_head_uint16(&frame);
    ESP_LOGD(TAG, "utc_offset = %x", output->body.utc_offset);

    body->clock_status = pop_buffer_head(&frame);
    ESP_LOGD(TAG, "clock_status = %x", output->body.clock_status);

    // Head now points to first OBIS element
    // and first element should be a struct
    int ret = dlms_parse_obis_objects(&output->objects, frame);
    if (ret < 0) {
        ESP_LOGE(TAG, "Could not parse OBIS objects");
        return ret;
    }

    ESP_LOGD(TAG, "parsed %d bytes of data", ret);
    frame += ret;

    size_t body_size = frame - head;

    return body_size;
}

static int obis_free_object(obis_object_t *object) {
    switch (object->type) {
        case OBIS_TYPE_STRUCT:
            for(int i = 0; i < object->data.size; ++i) {
                obis_free_object(&object->data.elements[i]);
            }
            free(object->data.elements);
            object->data.elements = NULL;
            object->data.size = 0;
            break;
        case OBIS_TYPE_OCTET_STRING:
            free(object->octet_string.string);
            object->octet_string.string = NULL;
            object->octet_string.len = 0;
            break;
        case OBIS_TYPE_VISIBLE_STRING:
            free(object->string.string);
            object->string.string = NULL;
            object->string.len = 0;
            break;
        default:
            break;
    }

    return 0;
}

int dlms_free_frame(dlms_frame_t *frame) {
    return obis_free_object(&frame->objects);
}

