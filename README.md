# TTK8 AMS IoT real time power usage sensor

This project was developed as a part of a course at NTNU called *Design of embedded systems*. The task I chose is described
in the [TASK.md](./TASK.md) file in this repo.

## PCB design

The PCB schematics and layout can be found in the `schematics` folder. The project is designed in KiCAD to adhere to the open source nature of the project.

The schematics folder contains a set of folders that contain each version of the board. Each revision is set when a board is fabricated.

It also contains a `libs` folder that holds the schematic and footprint libraries specific to this project.

Inside each of the revision folders a `gerber` folder is found where the fabrication output of that revision is put. This is the files that are sent to the
manifacturer and will be the way to reproduce an old revision if necessary. In the revN folder a schematic PDF should also be exported to be used for reference.


## Software

All the software is located in the `firmware` folder.

### Overview

The software for this project is based on the ESP32-S2 SDK. It

### How to build

To build the project use the `Makefile` located in the firmware folder. Build the entire project by running the `make all` command.

### How to flash

The board should be flashed using the `idf.py` tool. A `make flash` rule is exposed but it will assume that the UART port for the board is located at `/dev/ttyUSB0`


## Configuration


