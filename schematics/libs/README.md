# KiCAD project library

## Edge cuts on USB-C footprint

The included USB-C footprint requires a board cutout which is kinda a hack. This is from SO:

[The (unfortunate) best solution in KiCAD as of May, 2017 is to edit the footprint, place the cutout in the Eco1.User (engineering change order, which I like for this -- some people prefer Dwgs.User), save the footprint and the open the footprint library in a text editor. Once in the text editor, scroll down until you find the footprint and then COPY the cutout section from Eco1.User, paste and set the pasted layer to Edge.Cuts.](https://stackoverflow.com/questions/43949808/create-a-footprint-for-mounting-through-the-board-cut-in-kicad)

