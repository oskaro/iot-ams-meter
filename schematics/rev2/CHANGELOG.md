# Changelog for revision 2

## Additions

* Added CP2102 USB-to-UART bridge for easier programming.
* Added some ESD protection to USB connections.
* Added mounting holes.

## Changes

* Fix the enable pullup on the battery charger. 
* Switch out Micro USB for USB-C.
* Switch to smaller 0603 footprints because very little hand soldering is done.
* Change dip switch to a set of buttons for reset and booting.
* Increase size of board to accomodate the larger and added components.

