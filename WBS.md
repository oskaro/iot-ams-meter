@startwbs

*: **1.0.0 IoT AMS Meter**
Plan effort: 156
Real effort: 101
Deg.finish: 64.7%;

**: **1.1.0 Write specification**
Plan effort: 40
Real effort: 24
Deg.finish: 60%;
***: **1.1.1 Write system specification**
Plan effort: 10
Real effort: 7
Deg.finish: 70%;
***: **1.1.2 Write hardware specification**
Plan effort: 10
Real effort: 7
Deg.finish: 70%;
***: **1.1.3 Write software specification**
Plan effort: 10
Real effort: 5
Deg.finish: 50%;
***: **1.1.4 Write system acceptance criterias**
Plan effort: 10
Real effort: 5
Deg.finish: 50%;


**: **1.2.0 Hardware**
Plan effort: 45 
Real effort: 40
Deg.finish: 89%;
***: **1.2.1 Circuitry design**
Plan effort: 20
Real effort: 20
Deg.finish: 100%;
***: **1.2.2 PCB Layout**
Plan effort: 10
Real effort: 10
Deg.finish: 100%;
***: **1.2.3 Soldering**
Plan effort: 5
Real effort: 5
Deg.finish: 100%;
***: **1.2.4 Verification**
Plan effort: 10
Real effort: 5
Deg.finish: 50%;

**: **1.3.0 Software design**
Plan effort: 45
Real effort: 36
Deg.finish: 80%;
***: **1.3.1 Basic functionality **
Plan effort: 10
Real effort: 9
Deg.finish: 90%;
***: **1.3.2 Write maintainable interfaces**
Plan effort: 20
Real effort: 20
Deg.finish: 100%;
***: **1.3.3 Debugging**
Plan effort: 10
Real effort: 5 
Deg.finish: 50%;
***: **1.3.4 Verification**
Plan effort: 5
Real effort: 2
Deg.finish: 40%;

**: **1.4.0 Verification**
Plan effort: 26
Real effort: 1
Deg.finish: 0%;
***: **1.4.1 Run device for a while**
Plan effort: 5
Real effort: 1
Deg.finish: 0%;
***: **1.4.2 Measure and calculate power usage**
Plan effort: 10
Real effort: 0
Deg.finish: 0%;
***: **1.4.3 Verify "real time"-ness of device**
Plan effort: 1
Real effort: 0
Deg.finish: 0%;
***: **1.4.3 Verfy that acceptance criterias are met**
Plan effort: 10
Real effort: 0
Deg.finish: 0%;

@endwbs
